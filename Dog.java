public class Dog {
    
    private String name;
    private int energy;
    private int hunger;
    private int boredom;
    public static void main(String[] args) {
        Human human = new Human("George");
    }

    public Dog(String name){
        this.name = name;
        this.energy = 0;
        this.hunger = 0;
        this.boredom = 0;
    }

    public boolean takeNap(){
        if(this.hunger > 50 && this.boredom > 50){
            System.out.println("the dog was too bored or too hungry to nap");
            return false;
        }
        else{
            this.energy =+ 20;
            this.hunger =+ 5;
            this.boredom =+ 10;
            System.out.println("the dog napped successfully");
            return true;
        }
    }

    public void play(){
        if(this.boredom >= 0){
            this.boredom =- 30;
            this.hunger =+ 5;
            System.out.println("the dog played with the human.");
        }
    }

    public boolean playWith(Human human){
        if(this.energy < 50){
            System.out.println("the dog doesn’t have enough energy to play");
            return false;
        }

        else{
            this.energy =- 50;
            play();
            human.play();
            return true;
        }
    }
    public void eat() {
        this.hunger = hunger - 30;
        if(this.hunger < 0) {
            this.hunger = 0;
        }
        System.out.println("The Dog ate");
    }

}